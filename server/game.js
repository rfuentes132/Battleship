var Setting = require('./setting');
var Board = require('./board');


function Game () {
  this.setting = new Setting();
  this.p1Board = new Board();
  this.p2Board = new Board();
}


Game.prototype.onJoin = function (io, user) {

  if (this.setting.addUser(user)) {
    io.emit('joined', user);
  }



  if (this.setting.ready()) {
    io.emit('placement');
    // agregar peticion a users de setteo del tablero
    // tal vez sacar turn de aca
  }
};

  // io.emit('turn', this.setting.deductShift());

Game.prototype.onPlayed = function (io, socket, move) {
  // if move es incorrecto
  if (true) {} else{};
  socket.emit('none');

  // else
  var shift = this.setting.deductShift();

  console.log('played: ' + move.name);
  console.log('shift: ' + shift.name);

  io.emit('turn', shift);
};

module.exports = Game;
