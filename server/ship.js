function Ship (startCoord, size, position) {
	this.startCoord = startCoord;
	this.size = size;
	this.position = position;
}

module.exports = Ship;

// console.log('a'.length) -> 1
// console.log('a1'[0]) -> a

// var a = [0,1].length === 2;
// console.log(a); -> true