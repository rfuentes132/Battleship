function Board () {
	this.boardGrid = [];
	this.shotsGrid = [];
	this.ships = [];
}

Board.prototype.init = function(size) {
	size = size || 10
	for (var i = 0; i < size; i++) {
		this.boardGrid[i] = []
		this.shotsGrid[i] = []
		for (var j = 0; j < size; j++) {
			this.boardGrid[i][j] = 0
			this.shotsGrid[i][j] = 0
		};
	};
};

Board.prototype.shoot = function(coordX, coordY) {
	this.shotsGrid[coordY][coordX] = 'X';
};

Board.prototype.addShip = function(ship) {
	if (this.ships.length < 5){
		this.ships.push(ship);
		// agregar ship al tablero
		return true;
	}
	return false;
};

Board.prototype.ready = function() {
	return this.ships.length === 5;
};

module.exports = Board;

// b = new Board ();
// b.init();
// console.log(b);