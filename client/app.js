/*
 * Estoy usando readline-sync porque estoy interesado en frenar la ejecución del programa
 * desarrollando para backend con javascript esto sería mala idea,
 * como es un programa de consola este es un caso especial.
 */

// var readLineSync = require('readline-sync');
var prompt = require('prompt');
var socket = require('socket.io-client')('http://localhost:3000');

var user = {
  name: "",
  isMe: function (user) {
    return this.name === user.name;
  }
};

function onErr(err) {
  console.log(err);
  return 1;
}

socket.on('connect', function () {
  console.log('connected');

  prompt.start();
  prompt.get(['name'], function (err, result) {
    if (err) return onErr(err);
    user.name = result.name;
  });

  socket.emit('join', user);
});

socket.on('joined', function (userJoined) {
  if (user.isMe(userJoined)) {
    console.log('You have joined');
  } else {
    console.log('User ' + userJoined.name + ' has joined');
  }
});

socket.on('placement', function(){
  console.log('Ship placement');

  ships = [];
  var properties = [
    {
      // esta mierda de description no sirve
      description: 'Starting coordinate:',
      name: 'coord', 
      validator: /^[a-f]{1}[0-9]{1,2}$/,
      warning: 'Sample coordinate "a1".',
      required: true
    },
    {
      description: 'Size:',
      name: 'size',
      type: 'integer',
      required: true
    },
    {
      description: 'Position:',
      name: 'position',
      validator: /[vh]/ig,
      required: true
    }
  ];


  prompt.start();
  for (var i = 0; i <= 5; i++) {
    console.log('Ship '+ i);
    prompt.get(['Coordinate'], function (err, result) {
      if (err) return onErr(err);
      ship = {
        result.name,
      }
      
    });
  };

  // socket.emit('placedships', ships);
  // usar propmt aqui para enviar la posicion de los barcos
})

socket.on('turn', function (userTurn) {

  if (user.isMe(userTurn)) {

    var move = readLineSync.question('Play: ', {defaultInput: 'a1'});
    socket.emit('played', {name: user.name, move: move});

  } else {
    console.log('user '+ userTurn.name + ' is playing');
  }

});


socket.on('none', function () {
  // channel didactico, acá pueden ver que se puede transmitir solo a un cliente.
  console.log('none');
});





