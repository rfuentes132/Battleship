// var prompt = require('prompt');

// prompt.start();

// prompt.get(['username', 'email'], function (err, result) {
//   if (err) return onErr(err);
//   console.log('Command-line input received:');
//   console.log('  Username: ' + result.username);
//   console.log('  Email: ' + result.email);
// });


var prompt = require('prompt');

prompt.message = "";

// var properties = [
//   {
//     name: 'username', 
//     validator: /^[a-zA-Z\s\-]+$/,
//     warning: 'Username must be only letters, spaces, or dashes'
//   },
//   {
//     name: 'password',
//     hidden: true
//   }
// ];

var schema = {
	properties: {
	  coord: {
	    // name: 'coord', 
	    description: 'Starting coordinate',
	    validator: /^[a-f]{1}[0-9]{1,2}$/,
	    warning: 'Sample coordinate "a1".',
	    required: true
	  },
	  size: {
	    description: 'Size:',
	    // name: 'size',
	    validator: /^[1-5]$/,
	    type: 'integer',
	    required: true
	  },
	  position: {
	    description: 'Position:',
	    // name: 'position',
	    validator: /[vh]/ig,
	    required: true
	  }
	}
};

prompt.start();

prompt.get(schema, function (err, result) {
  if (err) { return onErr(err); }
  console.log('Command-line input received:');
  console.log('  Username: ' + result.username);
  console.log('  Password: ' + result.password);
});


function onErr(err) {
  console.log(err);
  return 1;
}
